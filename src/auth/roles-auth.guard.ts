import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable, UnauthorizedException } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { JwtService } from "@nestjs/jwt";
import { Observable } from "rxjs";
import { ROLES_KEY } from "./roles-auth.decorator";

@Injectable()
export class RolesAuthGuard implements CanActivate {
    constructor(private jwtService: JwtService, private reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const req = context.switchToHttp().getRequest();

        try {
            const reqRoles = this.reflector.getAllAndOverride(ROLES_KEY, [
                context.getHandler(),
                context.getClass(),
            ]);

            if (!reqRoles) {
                return true;
            }

            const httpHeader = req.headers.authorization;
            const bearer = httpHeader.split(' ')[0];
            const token = httpHeader.split(' ')[1];

            if (bearer != 'Bearer' || !token) {
                throw new UnauthorizedException('Пользователь не зарегистрирован');
            }

            const user = this.jwtService.verify(token);
            req.user = user;
            return user.roles.some(role => reqRoles.includes(role.value));
        } catch (e) {
            throw new HttpException('У пользователя нету доступа !!! ', HttpStatus.FORBIDDEN);
        }
    }
}