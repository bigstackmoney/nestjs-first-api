import { IsString, Length, IsEmail } from "class-validator";

export class CreateUserDto {
    @IsString({ message: 'Должно быть строкой' })
    @IsEmail({}, { message: 'Некоректный email' })
    email: string;

    @IsString({ message: 'Должно быть строкой' })
    @Length(6, 16, { message: 'Не меньше 6 и не больше 16' })
    password: string;
    name: string;
    surname: string;
}