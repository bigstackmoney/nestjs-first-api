

export class BannedUserDto {
    readonly userId: number;
    readonly banned: boolean;
    readonly reason: string;
}